import React from "react";
import { Route, Switch } from "react-router-dom";
import About from './About.jsx';
import Contact from './Contact.jsx';
import Services from './Services.jsx';
import Navebar from './Navebar.jsx';


const Apps =() =>{

    const Error = ()=>{
        return <h1>Opps ! Page Note Found</h1>;
    };

    return (
      <>
      <Navebar/>
        <Switch >
            <Route exact path="/" component={About} />
            <Route path="/services" component={Services} />
            <Route path="/contact" component={Contact} />
            <Route component={Error} />
            
        </Switch>


      </>  
    );
    
};
export default Apps;
