const Sdata = [
    {
         id: 1,
         sname: "Dark",
         imgsrc: "https://i2.wp.com/foxexclusive.com/wp-content/uploads/2020/07/dark-e1595870682431.jpg?fit=640%2C360&ssl=1" ,
         title:"A Netflix Original Series ",
         link:"https://www.netflix.com/pk/title/80100172" ,
    },
    {
         id: 2,
         imgsrc: "https://occ-0-1722-1723.1.nflxso.net/dnm/api/v6/evlCitJPPCVCry0BZlEFb5-QjKc/AAAABZfj3AT25X5O0h3mw_EO2YLPxctxuC_o4rOD8URUJxSKFHcZEkEWkBzPEax2_9PRPUWe55wQMnaOubMBJByD-XPImrLfHOOD8B2-ulS7frSlz04CMPmlVJFMDh6Cmw.jpg" ,
         title:  "A Netflix Original Series ",
         sname:  "Extracurricular",
         link:  "https://www.netflix.com/pk/title/80990668",
    },
    {
         id: 3,
         imgsrc:  "https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/38145f58808319.5bf89d101606e.jpg", 
         title:   "A Netflix Original Series ",
         sname:   "Stranger Things",
         link:    "https://www.netflix.com/pk/title/80057281",
    },
    {
         id: 4,
         imgsrc:  "https://i.pinimg.com/736x/c2/b0/b8/c2b0b8cea81aac9bf73c65d82ac25d9b.jpg", 
         title:   "Amazon Original Series ",
         sname:   "The Vampire Diaries",
         link:    "https://www.amazon.com/The-Vampire-Diaries-Season-8/dp/B01LYJSLMU",
    },
    {
          id: 5,
          imgsrc:  "https://i.pinimg.com/474x/fe/fa/7d/fefa7dde60dfd3c44fb3765e5f345d34.jpg", 
          title:   "A Netflix Original Series ",
          sname:   "My First First Love",
          link:    "https://www.netflix.com/pk/title/81026700",
    },
]
export default Sdata;