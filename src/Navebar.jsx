import React from "react";
import { NavLink } from "react-router-dom";

const Navebar =() =>{

    return (
        <>
      
        <div className="menu">
            <NavLink className="abs" exact activeClassName="active_class" to="/"> About Us </NavLink>
            <NavLink className="abs" exact activeClassName="active_class" to="/services"> Services </NavLink>
            <NavLink className="abs" exact activeClassName="active_class" to="/contact"> Contact </NavLink>
            <br/><br/>
        </div> 
        </>
    );
};
export default Navebar;
